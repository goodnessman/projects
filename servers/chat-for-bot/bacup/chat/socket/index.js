
var log = require('libs/log')(module);
var config = require('config');
var connect = require('connect');
var async = require('async');
var cookie = require('cookie');
var sessionStore = require('libs/sessionStore');
var HttpError = require('../error').HttpError;
var User = require('../models/user').User;

function loadSession(sid, callback) {
	sessionStore.load(sid, function (err, session) {
		if (arguments.length == 0) {
			return callback(null, null);
		}else {
			return callback(null, session);
		}
	});
}



function loadUser(session, callback) {
	if (!session.user) {
		log.debug('Session %s is anonymous', session.id);
		console.log('Session '+session.id+' is anonymous');
		return callback(null, null);
	}

	log.debug('retrieving user', session.user);

	User.findById(session.user, function (err, user) {
		if (err) callback(err);

		if (!user) {
			return callback(null, null);
		}
		// log.debug('user findById result: '+user);
		console.log('user findById result: '+user);
		callback(null, user);
	});
}

module.exports = function (server) {
	var io = require('socket.io')({
	    'origins': 'localhost:*',
	    'logger': log
	}).listen(server);

	io.use(function (socket, next) {
		var handshake = socket.request;

	  async.waterfall([
	  	function (callback) {
	  		handshake.cookies = cookie.parse(handshake.headers.cookie || '');
	  		var sidCookie = handshake.cookies[config.get('session:key')];
	  		var sid = connect.utils.parseSignedCookie(sidCookie, config.get('session:secret'));

	  		loadSession(sid, callback);
	  	},
	  	function (session, callback) {
	  		if (!session) {
	  			callback(new HttpError(401, "No session"));
	  		}else {
	  			handshake.session = session;
	  			loadUser(session, callback);
	  		}
	  	},
	  	function (user, callback) {
	  		if (!user) {
	  			callback(new HttpError(403, "Anonymous session may not connect"))
	  		}

	  		socket.handshake.user = user;
	  		callback(null);
	  	}
	  ], function (err) {
	  	if (!err) {
	  		return next();
	  	}

	  	if (err instanceof HttpError) {
	  		return new HttpError(401, "No session");
	  	}

	  	next(err);
	  });

	  
	});

	io.on('session:reload', function (sid) {
		var clients = io.clients();

		clients.forEach(function (client) {
			if (clienthandshake.session.id != sid) return;

			loadSession(sid, function (err, session) {
				if (err) {
					client.emit('error', 'server error');
					client.disconnect();
					return;
				}

				if (!session) {
					client.emit('logout');
					client.disconnect();
					return;
				}

				client.handshake.session = session;
			})
		})
	});



	io.on('connection', function(socket){

		console.log('socket.handshake.user '+socket.handshake.user)
		
		var username = socket.handshake.user.get('username');

		socket.broadcast.emit('join', username);

		socket.on('message', function (text, cb) {
		  socket.broadcast.emit('message', username, text);
		  cb && cb();
		});

		socket.on('disconnect', function () {
		  socket.broadcast.emit('leave', username);
		});

	});

	return io;
}  